export function TestDownload() {
  const [triggerDowload, setTriggerDownload] = useState(false);

  useEffect(() => {
    // console.log(triggerDowload);

    if (triggerDowload) {
      fetch("http://192.168.1.103:3001/download", {
        method: "GET",
      })
        .then((response) => {
          console.log(response);
          return response.blob();
        })
        .then((blob) => {
          // Create blob link to download
          const url = window.URL.createObjectURL(
            new Blob([blob], { type: blob.type })
          );
          const link = document.createElement("a");
          link.href = url;
          link.setAttribute("download", `FileName2.pdf`);

          // Append to html link element page
          document.body.appendChild(link);

          // Start download
          link.click();

          // Clean up and remove the link
          document.body.removeChild(link);
          setTriggerDownload(false);
        });
    }
  }, [triggerDowload]);
  return (
    <div>
      Hello World
      <button onClick={() => setTriggerDownload(true)}>Save file</button>
    </div>
  );
}
