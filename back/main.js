const express = require("express");
var cors = require("cors");

const app = express();

app.use(cors());

app.get("/download", (req, res) => {
  let resolve = require("path").resolve;

  res.set("Content-Disposition", 'attachment; filename:"test.pdf" ');
  res.set("Content-Type", "application/octet-stream");
  res.sendFile(resolve("./test.pdf"));
});

app.get("/", (req, res) => {
  res.send("Hello World!");
});

app.listen(3001, () => {
  console.log("Example app listening on port 3001!");
});
